<?php
/**
 * @file
 * Defines the User Narrative class
 *
 * Each instance of the class stores an array of string definitions as per the location (module directory) and type (file subgroup) specified in the constructor.
 * User Interface strings are returned through the public t() method.
 * 
 * This class was written by Michael Keara (User Advocate) - http://www.tuag.ca
 * 
 */

 
/**
 * Class: user_narratives
 */
class user_narratives  {
  private $string_resources = NULL;   // storage for the string arrays. Set when a string function is first used and remains 'static' for lifetime of object.
  
  // Set by constructor
  private $language = '';             // operating language - set by constructor
  private $location = NULL;           // contains the location (currently a directory) containing the resource (currently a file)
  private $active_type;               // Keeps track of the module that contains string resources. Static for the lifetime of the object  
  private $fn = NULL;                 // array of names to external functions (avoids direct dependency on calling environment)
  
  /**
   * Constructor
   * Creates the object
   * Parameters
   * @param mixed $location - required path for location of resource
   * @param mixed $language - language for the strings retrieved
   * @param mixed $module - used to identify the location of the resource file
   * @param mixed $type - identifier of the string subgroup (used to derive a file name)
   * 
   * Note: We may want to put some checking on the passed parameters.
   */
  function user_narratives($location, $lang='en', $type='common', $fn_array=NULL) {
    $this->language = $lang;
    $this->location = $location;
    $this->active_type = $type;
    $this->fn = $fn_array;
    
    $this->string_resources = array();
    
  }

  /**
  * returns the presentation string for a given key and language and handles placeholders
  * 
  * @param mixed $key - the semantic key that identifies the string instance
  * @param mixed $args - placeholder arguments (as per Drupal's t() function)
  * @param mixed $langcode - native language code
  * @return string - the presentation string
  */
  public function t($key, $args = array(), $langcode = NULL) {
    $string_array = $this->get_string_resource($langcode);

    if (!isset($string_array)) {
      return 'No resource file for: ' . $key;
    }
    // handle sub classed keys - if a valid match is found replace the $key
    $parts = explode('__', $key);
    $subparts = $parts;
    for ($i = count($parts); $i > 1; $i--) {
      // for each subclassed key found, look for overridden string definitions that match it
      $subkey = implode('__', $subparts);
      if (isset($string_array[$subkey])) {
        $key = $subkey;
        break;
      }
      unset($subparts[$i-1]);
    }

    $string = $string_array[$key];    
    
    if (empty($string)) {
      return ($key);
    }
    else {
      // handle placeholder replacement
      if (empty($args)) {
        return $string;
      }
      else {
        // Transform arguments before inserting them.
        foreach ($args as $arg_key => $value) {
          switch ($arg_key[0]) {
            case '@':
              // Escaped only.
              $args[$arg_key] = call_user_func($this->fn['@'], $value);               
              break;

            case '%':
            default:
              // Escaped and placeholder.
              $args[$arg_key] = call_user_func($this->fn['%'], 'placeholder', $value);
              break;

            case '!':
              // Pass-through.
          }
        }
        return strtr($string, $args);
      }
     return $string; 
    }
  }  

  /**
  * Returns the array containing the desired string resources
  * 
  * @param mixed $language - language for the strings retrieved
  */
  private function get_string_resource($langcode = NULL) {
    if (empty($langcode)) {
      $langcode = $this->language;
    }
      
    if (!isset($this->string_resources[$this->active_type][$langcode])) {
      $this->string_resources[$this->active_type][$langcode] = $this->load_resource($this->location, $this->active_type, $langcode);
    }
    
    return $this->string_resources[$this->active_type][$langcode];
  }

  
  /**
  * Derives a string resource filename and loads file into memory
  * 
  * @param mixed $location - used to identify the location of the resource file
  * @param mixed $type - identifier of the string subgroup (used to derive a file name)
  * @param mixed $language - language for the strings retrieved
  */
  private function load_resource($location, $type, $language) {
    $file_name = 'string_data_' . $type . '_' . $language . '.inc';
    $filepath = $location . '/' . $file_name;
    
    if (file_exists($filepath)) {
      include $filepath; 
      // $string_array is in scope when file is included
      return $_string_array;    
    }
    
   return NULL;
  }  

  
}


 
 
