<?php 
/**
 * @file
 * Example string resource file
 * 
*/

$_string_array  =  array(

'STR_MGR_HELP_INTRO'              => "Use the string manager to precisely define user interface strings.",
'STR_MGR_HELP_NEW_STRINGS'        => "Create new strings by adding items to this array",
'STR_MGR_HELP_NEW_FILES'          => "Create more string resource files to organize string definitions. You can place the string resource files for specific modules into the respective module directories.",

);

