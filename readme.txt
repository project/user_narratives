User Narratives Module - Readme.txt

===== PURPOSE OF USER NARRATIVES ======
The User Narratives module (AKA 'Narratives') allows systematic control of words used in user interfaces in order to permit UX designers to build UI textual prompts and cues that are finely tuned to specific usage contexts. We call these textual cues 'user narratives'.

===== BACKGROUND INFO: SOME BASICS (for non-developers) ======
The sets of words we see on the screen are refered to as 'strings' by developers. A string is a set of characters bounded by quotes. Like this:

"Hello, world"

In Drupal, these strings exist throughout the code. The standard Drupal practice is to put these strings into a special function, the 't' function which looks like this

$output = t("Hello, world");

The t() function does 2 things:
- It helps you find UI strings
- It provides a means of getting versions of these strings translated into different languages

===== THE USER NARRATIVE ALTERNATIVE ======
The User Narrative module allows for fine tuning of on screen words in two additional dimensions, beyond basic spoken language (en, fr etc). Narratives allows for further precision to accomodate Idioms and User Roles.

IDIOMS:
But the t() function does not allow UX designers to modify those strings in idiomatic or role-specific contexts. An example of idiomatic language is an interface that allows people to register for music tests in 2 different regional contexts. In Cnada these tests are called 'exams' but in the USA they are called 'assessments'. 

USER ROLES:
The people doing the registration may be students, parents (on behalf of their child) or teachers (on behalf of their students). These are different User Roles and they represent different mindsets, or ways of looking at the registration task. Think about the language implications for each of these use cases:

Student: "Register for my exam"
Parent:  "Register for my child's exam"
Teacher: "Register for my student's exam"

Now think about what that means if you factor in the regional Idioms:
Canadian Student: "Register for my exam"
Canadian Parent: "Register for my child's exam"
Canadian Teacher: "Register for my student's exam"

American Student: "Register for my assessment"
American Parent: "Register for my child's assessment"
American Teacher: "Register for my student's assessment"

To make a User Interface well targeted (and more comfortabel for each type of user), you have to say one of these six things! So that's what we can do with this module. 

===== HOW IT WORKS =====
This can be accomplished with the User Narratives module because the technique for string definition extracts the user facing strings completely from the code. Instead the strings are defined in separate files known as string resource files. (In the future we will also support string resource database tables also). This is done through the use of 'semantic keys' which identify entries in a string resource file. Here's an example:

Instead of this:
$output = t("Register for my exam");

we do this:
$output = $un->t('PROMPT_REGISTER__'.$role);

In the string resource file we'd see something like this:
'PROMPT_REGISTER__STUDENT'    =>  "Register for my exam",
'PROMPT_REGISTER__PARENT'     =>  "Register for my child's exam",
'PROMPT_REGISTER__TEACHER'    =>  "Register for my student's exam",

===== IMPLEMENTATON METHODS =====
Because this module is an alternative to Drupal's core method for defining strings, the basic strategy for using it involves custom modules. There are two ways of approaching this: 
A: your custom module defines entire interfaces such as forms and blocks and uses the Narratives object directly
and/or
B: your custom module transforms other people's form based interfaces through hook_form_alter and essentially replaces strings defined in t() with strings defined through the USer Narratives object

In addition to defining or transforming the user interface through a custom module, you will also need to supply a means of accessing the string resource files. You cold do this directly within your custom UI module but in some cases you may want to create a separate custom string resource module. In that case you'd something like this:
 my_ui_module - where you can define all forms and blocks for the UI
 my_strings_module - where you keep all your strings in nice neat files. Later on we'll talk about the value of using multiple versions of this module.
 
Remember we talked about handling custom strings for idiomatic cultural differences? In some cases these are big enough distinctions that they are probably related to completely different web sites. This is where you'd benefit from using separate string resource modules like this:
my_cdn_strings_module
my_usa_strings_module

Remember, these are resource modules and they don't contain much - each has just the .info file and a /strings directory with your string files in it. So if you have a situation where you want to use a common code base but fine tune strings for different client installs, all you need to do is create a separate string resource module and swap in the right one for each client.

 
===== GOING DEEPER ===== 
The code in my_ui_module module might include a function something like this:

function my_ui_module_registration_thingie() {

  // First of all we need a piece of data about what task the user is doing. Are they registering for themselves, their child or their student?
  // We want to specify strings for different kinds of users - 'Roles'. 
  // So first we get a string that tells us what Role the user is currently assuming. 
  // Let's say this function returns one of these fixed strings 'STUDENT', 'PARENT' or 'TEACHER'
  $role = get_the_user_current_role();    

  // Now we can build the User Narrative object
  $un = user_narratives_obj('my_cdn_strings_module'); 
  
  // Now we get the user interface string from the $un object
  $main_prompt = $un->t('PROMPT_REGISTER__'.$role); 
  
  ...
  
  // add that UI string into your big output string
  $output .= $main_prompt;  
  
  ...
    
  return $output; // send it all back to whoever asked for your registration thingie
}


The builder function (user_narratives_obj()) can take 2 parameters: 
  $module - the name of the strings module where you are defining your string resource files. The user_narratives module will look for these files in a '/strings' subfolder inside that module directory.
  $type - you can specify a type (or subgroup) of strings to help with organizaton and efficiency. Examples might be 'common' (for strings you use in many places) or 'forms' for strings you use in your forms. The User Narrative object will use 'common' as the default value.


As we saw the $un object has its own version of a t() function:
$output = $un->t('LINK_REGISTER__'.$role);

That function takes one required parameter and 2 optional parameters:
  $key - the semantic key that you define in your module.
  $args - just like the Drupal t() function, $un->t() can take placeholder arguments to drop in variable values.
  $langcode - the language code such as 'en' or 'fr' for English and French respectively
  
===== PLACEHOLDERS AND ARGUMENTS =====
We like the placeholder replacement techniques of the t() function that we put the same code right inside $un->t(). So it works just like t():

  $date_string = $un->t('PROMPT_TODAYS_DATE', array('@year' => $year, '@month' => $month, '@day' => $day)); // $year, $month and $day are variables derived from a date function
  
In the string resource file we define the string as follows:

'PROMPT_TODAYS_DATE'         =>  "This is today's date: @month @day, @year",  // The order doesn't matter, the names of the variables does!

  

===== LANGUAGES AND FILE ORGANIZATION =====   
The Narratives object will automatically find the string for the active language so you don't need to specify that in the $un->t() call. But of course you are responsible for creating the file for that language version. The filename is important for all string resource files. Here's how they are composed:
string_data_common_en.inc

which breaks down like this:
'string_data_' - the prefix on all string resource files.
'common_' - the subgroup of strings
'en' - the language that the strings in this file are defined in

Other examples are:
string_data_common_fr.inc
string_data_forms_en.inc
string_data_forms_fr.inc

The same file names can be used for each of your string resource modules. So you might have this kind of file structure to support your Canadian and USA idiomatic narratives:

my_cdn_strings_module
  my_cdn_strings_module.info
  /strings
    string_data_common_en.inc
    string_data_common_fr.inc
    string_data_forms_en.inc
    string_data_forms_fr.inc
      
my_usa_strings_module
  my_usa_strings_module.info
  /strings
    string_data_common_en.inc
    string_data_common_sp.inc
    string_data_forms_en.inc
    string_data_forms_sp.inc

Notice how the non-English strings for the USA are Spanish whereas they are French in Canada.

One more thing, you can actually request a string in a different language by passing the language code into $un->t(). For example:
  $main_prompt = $un->t('PROMPT_REGISTER__'.$role); // returns an English string
  
  $help_for_french_users = $un->t('PROMPT_LINK_TO_FRENCH_PAGE__'.$role, NULL, 'fr');   // Returns a string in French (paramter 2 is null becasue there are no arguments required.

  
  
===== HANDLING ROLES BY OVERRIDING YOUR SEMANTIC KEYS =====
Remember the three types of users - student, parent and teacher? These can be described as User Roles and we can fine tune our strings for each of them through an override technique. So, assuming you have all your USA strings in a given resource file defined within a string resource module (and you Canadian string in another) you can fine tune the strings for the roles in both countries without having to change any code. This is done with overrides and that involves special forms of the semnatic keys. Here's what they look like:
'PROMPT_REGISTER__STUDENT'    =>  "Register for my exam",
'PROMPT_REGISTER__PARENT'     =>  "Register for my child's exam",
'PROMPT_REGISTER__TEACHER'    =>  "Register for my student's exam",

Notice the double underscore after PROMPT_REGISTER. This is used to specify a type of user (or Role). The Narrative object will find these subtypes and return the appropriate string. It will also allow you to use a default string definition without any Role distinction:.
'PROMPT_REGISTER'    =>  "Register for an exam",

So if you don't want to get into the Role level just yet, don't worry, just start with the basic string definition. You can always add the overrides later.

===== WHY ARE THEY CALLED SEMANTIC KEYS? =====
Good question. and this reveals another benefit of the Narratives module technique - you can articulate the *usage context* of each string which is very useful for translators and wordsmithers. The key contains clues about how the string is going to appear to the user. Here are some examples:

'MENU_XYZ'    - a string used in a menu item
'OPTION_XYZ'  - s string used in an options selector
'PROMPT_XYZ'  - a string used to inform the user about what they need to do.
'LABEL_XYZ'   - a string used beside a form input

And you can probably guess what these are for:
'FIELDSET_TITLE_XYZ'
'BTN_XYZ'
'LINK_XYZ'

With further input from the community we'll be putting together a list of agreed upon key prefixes that you can use - that way we'll be able to read each other's code.

Remember, the 'XYZ' part could semantically indicate something specific about the usage context and thus help make the key unique and understandable. So we don't say just 'PROMPT' but rather 'PROMPT_REGISTER'. Even better than this simplified example is something like 'PROMPT_REGISTER_EXAM' because it's more specific and less likely top collide with other kinds of registration related strings (like creating an account, or signing up for an event, etc). Don't worry about using a specific word like 'EXAM' in the key because it has no effect on the choice of idiomatic strings used in the UI. In other words this is valid:
'PROMPT_REGISTER_EXAM'  "Register for an exam",  // used in the Canadian string resource file
and
'PROMPT_REGISTER_EXAM'  "Register for an assessment",  // used in the USA string resource file













  

  



Various techniques can be used to identify and inject strings into the user interfaces but they all currently involve the need for a custom module to be built to do the work. In general the recomendation is to define clusters of strings for critical interfaces and to build one or more modules to manage these clusters. 

The user_narrative module allows you to create object that do the management of the strings. The idea is to define 

For general background information on UI strings and the concepts of string management used in this module, go to http://tuag.ca/articles/core/section1/role-oriented-design - there are a series of related articles decribing the full concept.





